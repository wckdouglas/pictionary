#!/usr/bin/env python

import sys
import requests
import random

class PictionaryError(ValueError):
    pass

class WordGenerator():
    '''
    Word generator:
    sourcing word database from:
    1. github: https://github.com/dangelov/Pictionary-Card-Generator
    2. curated chinese word database: http://www.tqpx.cn/NewsStd_823.html
    '''
    def __init__(self):
        '''
        input:
            word type: different levels, or chinese
            n: number of words to be generated 
        '''
        self.word_types = {'easy','hard','medium','objects',
                            'persons','verbs','chinese','bible'}
        self.datapath = 'data/'
        self.word_list = []
        self.word_type = None
        # test input
    
    def get_data(self, word_type = 'easy'):
        '''
        source words
        '''
        self.word_type = word_type
        if self.word_type not in self.word_types: 
            raise PictionaryError('Supported type: %s' %','.join(list(self.word_types)))

        #get data
        if self.word_type == 'chinese':
            self.generate_chinese()
        elif self.word_type == 'bible':
            self.generate_bible()
        else:
            self.generate_english()

    
    def generate(self, n = 10):
        '''
        randomly source from the word list
        '''
        if n > len(self.word_list):
            raise PictionaryError('Number of words cannot be larger than %i for type: %s' %(len(self.word_list), self.word_type))


        for i in range(n):
            j = random.randint(0, len(self.word_list)-1)
            word = self.word_list[j]
            yield(word)
 
    
    def generate_english(self):
        '''
        retrieving words from github database
        '''
        url = 'https://raw.githubusercontent.com/dangelov/Pictionary-Card-Generator/master/word-lists/{}.txt'.format(self.word_type)
        r = requests.get(url)
        self.word_list = r.content.decode().split('\n')

    def generate_bible(self):
        '''
        get bible words
        '''
        self.get_word(self.datapath + '/bible.txt')
   
    def generate_chinese(self):
        '''
        Read in chinese database
        '''
        self.get_word(self.datapath + '/phrase.txt')
        self.get_word(self.datapath + '/database.txt')

    def get_word(self, filename):
        '''
        extend word list with file
        '''
        with open(filename, 'r') as words:
            self.word_list += [w.strip() for w in words] 

if __name__ == '__main__':
    generator = WordGenerator()
    if len(sys.argv) != 3:
        print('[usage] python %s <word type> <number of words>' %sys.argv[0], file=sys.stderr)
        print('Word type: %s' %', '.join(generator.word_types),file = sys.stderr)
        sys.exit()
    
    generator.get_data(word_type = sys.argv[1])
    for word in generator.generate(n = int(sys.argv[2])):
        print(word)
